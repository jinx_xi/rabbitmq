package com.mzr._01_hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2设置rabbitmq ip地址
        connectionFactory.setHost("192.168.254.130");

        // 3创建 Connection对象
        Connection connection = connectionFactory.newConnection();
        // 4创建 Chanel
        Channel channel = connection.createChannel();
        // 5设置队列属性
        /**
         * 第一个参数：队列名称
         * 第二个参数：队列是否要持久化,rabbitmq默认非持久化
         * 第三个参数：队列是否排他性
         * 第四个参数：队列是否自动删除
         * 第五个参数：队列是否设置一些额外的参数
         */
        channel.queueDeclare("01-hello", false, false, false, null);
        // 6发送消息
        /**
         * 第一个参数：交换机名称
         * 第二个参数：路由key
         * 第三个参数：消息属性
         * 第四个参数：消息内容(消息内容为字节数组)
         */
        channel.basicPublish("", "01-hello", MessageProperties.PERSISTENT_TEXT_PLAIN, "hello rabbitmq".getBytes());
        // 7关闭资源(使用 try-with-resources 语句 因为 Connection 和 Channel 都实现了 java.lang.AutoCloseable。 这样，我们就不需要在代码中显式关闭它们。)
        channel.close();
        connection.close();

    }
}
