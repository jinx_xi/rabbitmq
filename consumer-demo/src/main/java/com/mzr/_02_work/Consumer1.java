package com.mzr._02_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer1 {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2设置rabbitmq ip地址
        connectionFactory.setHost("192.168.254.130");

        // 3创建 Connection对象
        Connection connection = connectionFactory.newConnection();
        // 4创建 Chanel
        Channel channel = connection.createChannel();
        // 5设置队列属性
        channel.queueDeclare("02-work", false, false, false, null);
        //预取
        channel.basicQos(1);
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            String messages = new String(delivery.getBody());
            //手动签收消息
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(),false);
            System.out.println("消费者1消息为：" + messages);
        };
        channel.basicConsume("02-work", false, deliverCallback, consumerTag -> {
        });
    }
}
