package com.mzr._02_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2设置rabbitmq ip地址
        connectionFactory.setHost("192.168.254.130");

        // 3创建 Connection对象
        Connection connection = connectionFactory.newConnection();
        // 4创建 Chanel
        Channel channel = connection.createChannel();
        // 5设置队列属性
        /**
         * 第一个参数：队列名称
         * 第二个参数：队列是否要持久化,rabbitmq默认非持久化
         * 第三个参数：队列是否排他性
         * 第四个参数：队列是否自动删除
         * 第五个参数：队列是否设置一些额外的参数
         */
        channel.queueDeclare("02-work", false, false, false, null);
        //预取
        channel.basicQos(1);
        // 6使用chanel 去 rabbitmq 中去取消息进行消费
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            String messages = new String(delivery.getBody());
            //手动签收消息
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(),false);
            System.out.println("消费者消息为：" + messages);
        };
        channel.basicConsume("02-work", false, deliverCallback, consumerTag -> {
        });
    }
}
