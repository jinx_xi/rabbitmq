package com.mzr._04_routing;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer2 {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2设置rabbitmq ip地址
        connectionFactory.setHost("192.168.254.130");

        // 3创建 Connection对象
        Connection connection = connectionFactory.newConnection();
        // 4创建 Chanel
        Channel channel = connection.createChannel();
        // 5设置队列属性
        //channel.queueDeclare("02-work", false, false, false, null);
        /**
         * 第一个参数:交换机名字
         * 第二个参数:交换机类型
         */
        channel.exchangeDeclare("04-routing", "direct");
        //生成一个临时队列，因为是fanout模式，我们并不关心交换机绑定哪个队列
        //当我们没有向 queueDeclare（） 提供任何参数时，我们会创建一个具有生成名称的非持久、独占、自动删除队列
        String queue = channel.queueDeclare().getQueue();
        //与队列进行绑定,并且指定路由,可以指定多个
        channel.queueBind(queue, "04-routing", "debug");
        // 6使用chanel 去 rabbitmq 中去取消息进行消费
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String messages = new String(delivery.getBody());
            System.out.println("消费者2消息为：" + messages);
        };
        channel.basicConsume(queue, false, deliverCallback, consumerTag -> {
        });
    }
}
