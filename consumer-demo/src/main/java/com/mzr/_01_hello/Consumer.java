package com.mzr._01_hello;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2设置rabbitmq ip地址
        connectionFactory.setHost("192.168.254.130");

        // 3创建 Connection对象
        Connection connection = connectionFactory.newConnection();
        // 4创建 Chanel
        Channel channel = connection.createChannel();
        // 5设置队列属性
        /**
         * 第一个参数：队列名称
         * 第二个参数：队列是否要持久化,rabbitmq默认非持久化
         * 第三个参数：队列是否排他性
         * 第四个参数：队列是否自动删除
         * 第五个参数：队列是否设置一些额外的参数
         */
        channel.queueDeclare("01-hello", false, false, false, null);
        // 6使用chanel 去 rabbitmq 中去取消息进行消费
        /**
         * 第一个参数:队列名称
         *第二个参数:是否自动签收
         */
        /*channel.basicConsume("01-hello", true, new DeliverCallback() {
         *//**
         *水当消息从 mq中取出来了会回调这个函数
         *消费者消费消息就在这个handle中去进行处理
         *//*
            public void handle(String consumerTag, Delivery message) throws IOException {
                System.out.printf("消息内容为：" + new String(message.getBody()));
            }
        }, new CancelCallback() {
            *//**
         * 当消息取消了会回调这个方法
         *//*
            public void handle(String consumerTag) throws IOException {
                System.out.printf("1111");
            }
        });*/
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String messages = new String(delivery.getBody());
            System.out.println(messages);
        };
        channel.basicConsume("01-hello", true, deliverCallback, consumerTag -> {
        });
    }
}
