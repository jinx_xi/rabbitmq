package com.mzr._03_pubsub;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer2 {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1创建一个连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2设置rabbitmq ip地址
        connectionFactory.setHost("192.168.254.130");

        // 3创建 Connection对象
        Connection connection = connectionFactory.newConnection();
        // 4创建 Chanel
        Channel channel = connection.createChannel();
        // 5设置队列属性
        //channel.queueDeclare("02-work", false, false, false, null);
        /**
         * 第一个参数:交换机名字
         * 第二个参数:交换机类型
         */
        channel.exchangeDeclare("03-pubsub1", "fanout");

        String queue = channel.queueDeclare().getQueue();

        channel.queueBind(queue, "03-pubsub1", "");
        // 6使用chanel 去 rabbitmq 中去取消息进行消费
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String messages = new String(delivery.getBody());
            System.out.println("消费者2消息为：" + messages);
        };
        channel.basicConsume(queue, false, deliverCallback, consumerTag -> {
        });
    }
}
